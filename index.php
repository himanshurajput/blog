<?php
require_once "config.php";
$query = "SELECT `id`,`title` FROM `blogs`";
$result = $connection-> query($query);
$listofallblogs=$result->fetch_all(MYSQLI_ASSOC);
require "header.php";
?>
<!doctype html>
<html>
<head lang="en">
 <meta charset="UTF-8">
 <title>codekamp/list of all blogs</title>
 </head>
 <body>
 <h1>List of all blogs</h1>
 <ul>
 <?php
 foreach($listofallblogs as $blog)
echo '<li><a href="view.php?id=' .$blog['id'] .'">' . $blog['title'] .'</a></li>'; 
 ?>
 </ul>
 <a href="create.php">Create a new blog</a>
</body>
 </html>
